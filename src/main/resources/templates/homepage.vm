#* @vtlvariable name="webResourceManager" type="com.atlassian.plugin.webresource.WebResourceManager" *#
#* @vtlvariable name="applicationProperties" type="com.atlassian.sal.api.ApplicationProperties" *#
#* @vtlvariable name="platformComponents" type="java.util.Map<java.lang.String, java.lang.String>" *#
<html>
<head>
    <title>$i18n.getText("homepage.title")</title>
    <meta name="decorator" content="atl.admin">
    $webResourceManager.requireResource("com.atlassian.devrel.developer-toolbox-plugin:homepage-resources")
</head>
<body>

<div id="toolbox-container">

    #set($baseUrl = $applicationProperties.baseUrl)

    <fieldset class="hidden parameters">
        <input type="hidden" id="block_collapsed"
               value='$webResourceManager.getStaticPluginResource("com.atlassian.devrel.developer-toolbox-plugin:homepage-resources", "images/icon_blockcollapsed.png")'>
        <input type="hidden" id="block_expanded"
               value='$webResourceManager.getStaticPluginResource("com.atlassian.devrel.developer-toolbox-plugin:homepage-resources", "images/icon_blockexpanded.png")'>
        <input type="hidden" id="baseUrl"
               value="$baseUrl"/>
        <input type="hidden" id="product" value="$applicationProperties.displayName.toLowerCase()">
    </fieldset>

    <div id="top-controls-container">
        <img id="toolbox-logo"
             src='$webResourceManager.getStaticPluginResource("com.atlassian.devrel.developer-toolbox-plugin:homepage-resources", "images/atlassian-logo.png")'
             alt="" border="0">
    </div>

    <div id="developer-tools-content">
        <div id="developer-tools-container" class="aui-toolbar">
            <div class="toolbar-split toolbar-split-left">
                <ul class="toolbar-group">
                #if ($i18nAvailable)
                    <li class="toolbar-item"><a class="toolbar-trigger #if($i18nActive)active#end" href="#" id="i18n-on">Highlight i18n</a></li>
                    <li class="toolbar-item"><a class="toolbar-trigger #if(!$i18nActive)active#end" href="#" id="i18n-off">Unhighlight i18n</a></li>
                #else
                    <li class="toolbar-item disabled">$applicationProperties.displayName doesn't support i18n highlighting.</li>
                #end
                </ul>
            </div>
            <div class="toolbar-split toolbar-split-right">
                <ul class="toolbar-group">
                    <li class="toolbar-item"><a class="toolbar-trigger" href="$baseUrl/plugins/servlet/fastdev">Scan and reload FastDev</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="current-versions-content">
        <div id="current-product-version" class="infographic-container">
            <div class="aui-message info">
                <p class="title"><strong>You're running $applicationProperties.displayName version:</strong></p>
                <span class="current-version">$applicationProperties.version</span>
                <p><a href="http://www.atlassian.com/software/$applicationProperties.displayName.toLowerCase()/download?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Download the latest <span id="latest-product-version"></span></a></p>
            </div>
        </div>

        <div id="current-sdk-version" class="infographic-container">
            <div class="aui-message info">
                <p class="title"><strong>You're using Atlassian SDK version:</strong></p>
                <span class="current-version">$sdkVersion</span>
                <p><a href="https://developer.atlassian.com/display/DOCS/Atlassian+Plugin+SDK+Documentation?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Download the latest</a></p>
            </div>
        </div>
    </div>

    <div id="toolbox-content">
        <table id="toolbox-content-table" class="">
            <tr>
                <td>
                    <h2>Development Tools</h2>

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="$baseUrl/plugins/servlet/upm#osgi">OSGi Plugins Console</a></p>
                    <div class="docs-list">
                        <p>
                           The <a href="https://developer.atlassian.com/display/UPM/Universal+Plugin+Manager?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Universal Plugin Manager</a>'s
                           OSGi-level view of all plugins, package exports/imports, and shared components. If you want to access the OSGi
                           container directly, you can use the <a class="toolbox-item" href="$baseUrl/plugins/servlet/system/console">Apache Felix web console</a>
                           (using <code>admin/admin</code> to log in), but you can damage your installation by doing so.
                        </p>
                    </div>

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="$baseUrl/plugins/servlet/fastdev">FastDev Console</a></p>
                    <div class="docs-list">
                        <p>
                           Use <a href="https://developer.atlassian.com/display/DOCS/Automatic+Plugin+Reinstallation+with+FastDev?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">FastDev</a> to reload
                           your code changes in this plugin without restarting $applicationProperties.displayName.
                        </p>
                    </div>

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="$baseUrl/plugins/servlet/restbrowser">REST API Browser</a></p>
                    <div class="docs-list">
                        <p>Experiment with every installed REST or JSON-RPC service in this instance of $applicationProperties.displayName.</p>
                    </div>

##                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/>Show Available Plugin Module Types</a></p>
##                    <div class="docs-list">
##                        <p></p>
##                    </div>

                #if ($speakeasyAvailable)

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="$baseUrl/plugins/servlet/speakeasy/user">Speakeasy Console / Create New Extension</a></p>
                    <div class="docs-list">
                        <p>
                           Manage your existing <a href="https://developer.atlassian.com/display/SPEAK/Speakeasy?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Speakeasy</a> extensions
                           or create a new one.
                        </p>
                    </div>

                #end

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/>Show Platform Components (and versions)</a></p>
                    <div class="docs-list">
                        <p>
                           Shows which versions of the <a href="https://developer.atlassian.com/display/DOCS/Atlassian+Plugin+Development+Platform?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Atlassian Platform</a>
                           components are used in this product.
                        </p>
                        <ul>
                    #foreach($platformComponent in $platformComponents.keySet())
                        <li>$platformComponent -- <b>$platformComponents.get($platformComponent)</b></li>
                    #end
                        </ul>
                    </div>

                #if ($aoAvailable)

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="$baseUrl/plugins/servlet/active-objects/tables/list">ActiveObjects Console</a></p>
                    <div class="docs-list">
                        <p>
                           Provides access to the currently active <a href="https://developer.atlassian.com/display/AO/Active+Objects?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">ActiveObjects</a>
                           tables and their data.
                        </p>
                    </div>

                #end

                #if ($pdeAvailable)

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="$baseUrl/plugins/servlet/pde">Plugin Data Console</a></p>
                    <div class="docs-list">
                        <p>
                           The <a href="https://plugins.atlassian.com/plugin/details/44369?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Plugin Data Editor</a> provides access to data stored
                           in the product with <a href="http://confluence.atlassian.com/pages/viewpage.action?pageId=164214?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Bandana</a> or the
                           <a href="https://developer.atlassian.com/display/SAL/SAL+Services#SALServices-!package2.gif!%7B%7Bcom.atlassian.sal.api.pluginsettings%7D%7D?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Shared
                           Application Layer (SAL) PluginSettings</a> API.
                        </p>
                    </div>

                #end

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="$baseUrl/$systemInfoLink">System Information</a></p>
                    <div class="docs-list">
                        <p>Links to $applicationProperties.displayName's system information page for diagnostics.</p>
                    </div>

                    <p class="toolbox-link"><a href="#" class="toolbox-twizzler"><img class="triangle" src=""/></a><a class="toolbox-item" href="http://checkup.atlassian.com/?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Atlassian Plugin Checkup</a></p>
                    <div class="docs-list">
                        <p>
                            The <a href="http://checkup.atlassian.com/">Atlassian Plugin Checkup</a> tool can analyze your plugin's code and generate a report
                            on which uses of Atlassian APIs are likely to break or cause trouble in different versions of $applicationProperties.displayName.
                        </p>
                    </div>
                </td>
                <td>
                    <h2>Documentation</h2>
                    <p class="toolbox-link"><a href="http://developer.atlassian.com/?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">All Developer Documentation</a></p>
                    <p class="toolbox-link"><a href="http://developer.atlassian.com/display/$space?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">$applicationProperties.displayName Developer Documentation</a></p>
                    <p class="toolbox-link"><a href="http://developer.atlassian.com/static/javadoc/$applicationProperties.displayName.toLowerCase()/$applicationProperties.version?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox"
                            >JavaDoc for this version of $applicationProperties.displayName</p>
                    <p class="toolbox-link"><a href="https://developer.atlassian.com/display/AUI/Atlassian+User+Interface+%28AUI%29+Developer+Documentation?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox"
                            >Atlassian User Interface Documentation</a></p>
                    <p class="toolbox-link"><a href="http://docs.atlassian.com/aui/latest/sandbox/?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Atlassian User Interface Sandbox</a></p>
                    <p class="toolbox-link"><a href="https://developer.atlassian.com/display/MARKET/Atlassian+Plugin+Exchange?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox"
                            >Share your plugin on the Atlassian Marketplace</a></p>
                    <p class="toolbox-link">&nbsp;</p>
                    <h2>Get Help</h2>
                    <p class="toolbox-link"><a href="https://my.atlassian.com/email?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Get Developer Updates from Atlassian</a></p>
                    <p class="toolbox-link"><a href="https://answers.atlassian.com/?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox">Get Help at answers.atlassian.com</a></p>
                </td>
            </tr>
        </table>

    </div>

</div>

<div style="height:300px;"></div>
</body>
</html>