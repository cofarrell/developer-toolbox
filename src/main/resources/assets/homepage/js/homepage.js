AJS.toInit(function() {

    AJS.$.ajax({
        url: 'https://my.atlassian.com/download/feeds/current/' + AJS.params.product + '.json',
        dataType: 'jsonp',
        jsonpCallback: 'downloads',
        success: function(data) {
            var version = data[0].version;
            AJS.$("#latest-product-version").html('(' + version + ')');
        }
    });

    AJS.$('#i18n-on').click(function() {
        window.location.href = AJS.params.baseUrl + "/plugins/servlet/developer-toolbox?i18ntranslate=on";
    });

    AJS.$("#i18n-off").click(function() {
        window.location.href = AJS.params.baseUrl + "/plugins/servlet/developer-toolbox?i18ntranslate=off";
    });

    AJS.$("#fastdev-reload-button").click(function() {
        window.location.href = AJS.params.baseUrl + "/plugins/servlet/fastdev";
    });

    AJS.$('img.triangle').attr('src', AJS.params.block_collapsed);

    AJS.$('.toolbox-twizzler').click(function() {
        var self = AJS.$(this);
        self.parent().next('.docs-list').slideToggle("fast");
        if (self.hasClass('expanded')) {
            self.find('img.triangle').attr('src', AJS.params.block_collapsed);
            self.removeClass('expanded');
        } else {
            self.find('img.triangle').attr('src', AJS.params.block_expanded);
            self.addClass('expanded');
        }
        return false;
    });

});