package com.atlassian.devrel.rest.services;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Provides the ewadl resource.
 */
@Path("ewadl")
public class EwadlResource {

    private static final String EWADL_TEMPLATE = "/templates/ewadl.vm";

    private final TemplateRenderer renderer;

    public EwadlResource(TemplateRenderer renderer) {
        this.renderer = renderer;
    }

    @AnonymousAllowed
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getWadl() throws IOException {
        StringWriter xmlContainer = new StringWriter();
        renderer.render(EWADL_TEMPLATE, xmlContainer);
        return Response.ok(xmlContainer.toString()).build();
    }

    /**
     * Some new doc.
     *
     * @request.representation.example {"b":"b"}
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc "Blah blah blah"
     * @response.representation.200.example {"b":"b"}
     */
    @AnonymousAllowed
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response blah(Blah blah) throws IOException {
        return Response.ok("").build();
    }

    public static class Blah {
        private String b = "";
    }
}
